//
// Corso di Programmazione - Tutorato
// AA 2015/2016
// Facoltà di Informatica
// Università degli Studi di Roma Tor Vergata
//
// Data una matrice quadrata binaria, trovare la massima sottomatrice quadrata
// costituita da soli 1
// @ref Programmazione dinamica: https://it.wikipedia.org/wiki/Programmazione_dinamica
//
// @version 0.1.alpha
// @link http://www.gianlucarossi.net/programmazione-dei-calcolatori
//
// @author Daniele Pasquini
// @email daniele.pasquini@students.uniroma2.eu, psqdnl@hotmail.it, psqdnl@gmail.com
//

#include <stdio.h>

/**
 * Costanti: è possibile cambiare tali valori
 */
#define N 3
#define M 3

/*
 * Prototipi
 */
void massimaSottomatrice(int[][M]);
int minimo(int, int, int);


/**
 * @param x, y, z valori tra cui trovare il minimo
 * @return il minimo tra gli input
 */
int minimo(int x, int y, int z) {
    int min = x;
    if(min > y) {
        min = y;
    }
    if(min > z){
        min = z;
    }
    return min;
}

/**
 * Calcola la massima sottomatrice quadrata composta di soli 1
 * creando una matrice ausiliaria
 *
 * @param matrice di dimensioni fissate
 */
void massimaSottomatrice(int matrice[][M]) {
    int i, j;
    // matrice ausiliare
    int ausiliaria[N][M];
    // indici della sottomatrice più grande
    // s_massimo denota l'elemento massimo nella matrice ausiliaria
    int a_massimo, i_massimo, j_massimo;

    // Copia la prima colonna di matrice in ausiliaria
    for (i = 0; i < N; i++) {
        ausiliaria[i][0] = matrice[i][0];
    }

    // Copia la prima riga di matrice in ausiliaria
    for (j = 0; j < N; j++) {
        ausiliaria[0][j] = matrice[0][j];
    }

    for ( i = 1; i < N; i++) {
        for ( j = 1; j < N; j++) {
            // Se l'elemento in posizione i, j è uguale a 1 trova il minimo tra gli elementi
            // i, j-1 e i-1, j e i-1, j-1 della matrice ausiliaria
            // sommando ad esso 1
            if (matrice[i][j] == 1) {
                ausiliaria[i][j] = minimo(ausiliaria[i][j - 1], ausiliaria[i - 1][j], ausiliaria[i - 1][j - 1])+1;
            } else {
                ausiliaria[i][j] = 0;
            }
        }
    }

    // Stampa di ausiliaria
    printf("\nMatrice ausiliaria: \n");
    for ( i = 0; i < N; i++) {
        for (j = 0; j < M; j++) {
            printf(" %d ", ausiliaria[i][j]);
        }
        printf("\n");
    }

    a_massimo = ausiliaria[0][0];
    i_massimo = 0;
    j_massimo = 0;

    // Trova il massimo in ausiliaria
    for ( i = 0; i < N; i++) {
        for ( j = 0; j < M; j++) {
            if (a_massimo < ausiliaria[i][j]) {
                a_massimo = ausiliaria[i][j];
                i_massimo = i;
                j_massimo = j;
            }
        }
    }

    // Stampa la sottomatrice massima composta di soli 1
    printf("\nMatrice di dimensione: %d, %d\n\n", a_massimo, a_massimo);
    for(i = i_massimo; i > i_massimo - a_massimo; i--) {
        for(j = j_massimo; j > j_massimo - a_massimo; j--) {
            printf(" %d ", matrice[i][j]);
        }
        printf("\n");
    }
}
